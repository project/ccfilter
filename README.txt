
The ccfilter module - collection of popular filters.

Ccfilter: PHP inline code.
"PHP inline code" filter type - allows you to embed the PHP-code into the content.
Put your PHP-code between the tags [phpcode] and [/phpcode].
PHP-code between these tags MUST BE a valid PHP-code.
This can be useful, when you need to show your code and the result of this code
at the same time on one page. The result of the implementation of the code will
be placed between <div class="ccfilter phpcode"> ... </div>. Besides, you can
expand the definition of the class through an indication "class = Your_class".
For example, the result of the code between [phpcode class = Your_class] ... [/phpcode],
will be placed in <div class="ccfilter phpcode Your_class"> ... </div>.

Ccfilter: Collapse text.
"Collapse" filter type - the text entered between the tags [collapse] and [/collapse]
can be collapsed/expanded. If the opening tag has "collapsed" indication, the
original state of the text will be minimized. The indicating tag can be placed
in the opening tag after the "=". Example: [collapse collapsed = Click here to see the text].
If you do not specify an indication, it will use the the text defined in the filter settings.
It is also allowed to use [collapse collapsed title = Click here to see the text].

Ccfilter: Tooltip.
"Tooltip" filter type - the text entered between the tags [tooltip=title] and [/tooltip]
will pop-up as a hint text specified in the opening tag after the "=" on mouse over.
Tag [tooltip] can be replaced by synonyms [acronym] and [abbr].
In addition to the tag [tooltip], it is allowed to use [tooltip title=Here is your pop-up text].
You can also expand the definition of the class through an indication of
"class = Your_class". For example, the text entered between
[tooltip class = Your_class] ... [/tooltip],  will be placed between the
tags <span class="ccfilter tooltip Your_class"> ... </span>.

Ccfilter: Description.
"Description" filter type - the text entered between the tags [decription ]..[/description]
(or short form [d]...[/d]), will be placed between the tags <div class="ccfilter descr">
and </div>. If you indicate "inline" in the opening tag, the text will be placed
between the tags <span class="ccfilter descr"> and </span>. You can also expand
the definition of the class through an indication of "class = Your_class"
For example, the result of entering text between [description class = Your_class] ... [/description],
will be placed between the tags <div class="ccfilter desc Your_class"> and </div>.

Ccfilter: Smileys.
"Smileys" filter type  - converted text smileys to images.

Ccfilter: Hidden text.
"Hidden text" filter type - hides content for unregistered users.
Upon entering the text between the tags [hidden = value] and [/hidden], it will
be visible to registered users only.  If the "value" attribute is indicated,
this value will be returned instead of the hidden text. In addition to the tag
[hidden] it is allowed to use [hidden title = value]. In case of using the form
[hidden] ... [/hidden], the text defined in the filter settings will be displayed
for unregistered users. The result will be placed between the tags:
for registered users <div class="ccfilter hidden-text"> and </div>
for unregistered users <div class="ccfilter hidden-text hidded"> ... </div>
You can also expand the definition of the class through an indication of "class = Your_class".
For example, the result of entering text between [hidden class = Your_class] ... [/hidden],
will be placed in <div class="ccfilter hidden-text Your_class"> ... </div>.

Ccfilter: Auto tags.
Auto tags filter type - use {{ and }} (or other tags defined in ccfilter settings),
you word entered  between the this tags auto adding in vocabulary (vocabulary defined in settings ccfilter).


INSTALLATION INSTRUCTIONS
---
1. Extract this module to sites/[ all | {domain} ]/modules folder.
2. If use filter type ccfilter:smileys, download file http://hptel-prog.ru/sites/all/files/smileys.zip
   and extract into smileys folder in this module folder
3. If use filter type  ccfilter:tooltip download scripts and place into lib folder in this module folder
   (files: jquery.tooltip.pack.js,jquery.bigframe.pack.js,jquery.dimensions.pack.js)
   tooltip
   http://bassistance.de/jquery-plugins/jquery-plugin-tooltip
   bigframe
   http://plugins.jquery.com/node/46/release
   dimensions
   http://plugins.jquery.com/project/dimensions
   use last version this scripts
2. Login as the user who has administrator permissions (user/1).
3. Activate ccfilter in the "Filters" category,
   on the Administer > Site building > Modules page at admin/build/modules.

CONFIGURATION
---
1. In order to use the ccfilter, go to Administer > Site configuration > Input formats
   at admin/settings/filters

2. Choose the format, and include the necessary ccfilters in it.

3. Make sure that the HTML filter allows <span>, <div> and <fieldset> tags if you
   use "ccfiltet:collapse text", or that the filter you have chosen is placed
   after the HTML filter, depending on your choice of ccfilter filter.

4. If you use "ccfilter:php inline code" format, move its priority to the top
   in the filter queue at (admin/settings/filters/%/order).

5. Optional: Make some changes to module configuration at
   Administer -> Site configuration -> Ccilter (admin/settings/ccfilter)


KNOWN ISSUES
---
Please use the issue queue to report any issues you think need to be fixed.